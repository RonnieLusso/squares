# To Do #

- CanMove Indicatior UI

- Individual sprites UI

- Resources generated/turn UI

- Upkeep/turn UI

- Structures 

- Upgrade\activated Ability

- option for new hand

- status effects 


# Rule Set 2.0 #

**| The Basics (Battling)|**

- Categories: Deck | Hand | Graveyard | Resources | The Board | Cards | Characters

Deck 

- Your Deck is a collection of Cards you selected to Bring in to Battle

- This selection of cards is Shuffled at the start of the Battle

- Your Deck can not be greator then X number of cards 

- Your Deck can not be smaller then Y number of cards

- You cannot see your Deck once the Battle has begun 

- At the start of Your Turn a Card is pull from your Deck and Placed into your Hand

Hand

- Your Hand is a collection of Cards you have direct access to 

- You can Play any Card in your Hand Given you have enough Resources

- Your hand cannot hold more then X Number of cards 

Graveyard 

- Your Graveyard is a collection of Cards that have already been used

- After a Card is used it is then thrown into the Graveyard 

Resources 

- Resources are Gained at the start of every Turn and the Ammount you recieve is based on the Number Of Tiles you Control 

- Resources are Used to play cards 

- Your Resources Do Not Roll Over inbetween Turns 

The Board

- The Board is a Grid of Tiles The players are Trying to interact with 

- At the Start of the Game each Tile is Empty

- A Tile can be either Friendly or Hostile 

- A Friendly Tile generates Resources for you at the start of your Turn

- Certain Tiles can generate more Resources then others

- A Character Card must be placed on one of these tiles once Played

- A Character converts the Tile it is currently on to a Friendly one

- A Character can Jump onto an enemy Piece to Send it Back to that Players Hand

Cards

- There are Two types of Cards | Spell Cards | Character Cards

- Spell Cards perform a Temporary Effect on the Board

- Character Cards Create a Character On The Board When Played

- If the Character gets Knockedout then the Characters Card is Placed Back in the Players Hand, The Character Recieves Damage

- Cards have a Cost to play that Requires Resources

Characters 

- Characters are Character Cards that Have been Played onto the Board 

- Characters have a few Special Aspects | Movement | Stats | Passive Abilities | Active Abilities 

**| Character |**

Movement | Determines the way a character moves across The Board

- Meditate - Doesnt move

- Jump - Moves this Character directy to selected Tile, Knocks-out any enemy Character already on Tile

- Slide - Moves this Character across a selection of Tiles in order to end up at selected Tile, Slide stopped-short by both Friendly and Enemy Characters

- Slam - Acts just like a Jump, but typically allows for greator range, this Character takes damage equal to that of the current Health of any Character Knocked-out

- Charge - Acts just like a slide, but allows Knock-outs, this square takes damage equal to that of the current Health of any Character Knocked-out

- Sacrifice -  Acts just like a Jump, but also returns this Character back to Players Hand

- Transcend -  Acts just like a jump, but does Not allow Knock-outs

- Dart - Allows you to move twice in one turn, cannot knock out 

Stats | Base Stats | Directly Determines the effectiveness of a Character

- Attack - Determines the ammount of Damage to be inflicted behind every Trait that effects an Enemy Squares Health

- Health - Determines the ammount of Damage the Square is able to recieve before getting Eliminated

- Resitance - Determines the Percentage of Damage you recieve whenever Attacked

Stats | Growth Stats | Determines the Percent increase each Base Stat will recieve upon Level Up

- Attack Growth - Determines how much Attack will be increased upon Leveling Up

- Health Growth - Determines how much Health will be increased upon Leveling Up

- Resitance Growth -  Determines how much Resistance will be increased upon Leveling Up

Passive Abilites | An Effect that Applies to This Character Throughout its Exsistance

Active Abilities | An Effect that the Player can Trigger for Resources | Have cooldowns 