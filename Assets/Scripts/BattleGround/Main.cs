﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Main : MonoBehaviour {
    


	// Use this for initialization
	void Start () {
        CharacterData data1 = CharacterLoader.getCharacterData("Arachnid");
        CharacterData data2 = CharacterLoader.getCharacterData("Hornet");
        CharacterData data3 = CharacterLoader.getCharacterData("Fly");
        CharacterData data4 = CharacterLoader.getCharacterData("Ladybug");
        CharacterData data5 = CharacterLoader.getCharacterData("Gecko");
        CharacterData data6 = CharacterLoader.getCharacterData("Spider");
        List<Card> cards = new List<Card>();
        Card c = new CharacterCard(data6);
        Card c2 = new CharacterCard(data6);
        Card c3 = new CharacterCard(data6);
        Card c4 = new CharacterCard(data5);
        Card c5 = new CharacterCard(data5);
        Card c6 = new CharacterCard(data4);
        Card c7 = new CharacterCard(data4);
        Card c8 = new CharacterCard(data3);
        Card c9 = new CharacterCard(data3);
        Card c10 = new CharacterCard(data2);
        Card c11 = new CharacterCard(data2);
        Card c12 = new CharacterCard(data1);
        Card c13 = new CharacterCard(data1);
        cards.Add(c);
        cards.Add(c2);
        cards.Add(c3);
        cards.Add(c4);
        cards.Add(c5);
        cards.Add(c6);
        cards.Add(c7);
        cards.Add(c8);
        cards.Add(c9);
        cards.Add(c10);
        cards.Add(c11);
        cards.Add(c12);
        cards.Add(c13);
        cards.Add(new Swindled());
        cards.Add(new Swindled());
        cards.Add(new Reinforce());
        cards.Add(new Reinforce());

        List<Card> cards2 = new List<Card>();
        Card ac = new CharacterCard(data6);
        Card ac2 = new CharacterCard(data6);
        Card ac3 = new CharacterCard(data6);
        Card ac4 = new CharacterCard(data5);
        Card ac5 = new CharacterCard(data5);
        Card ac6 = new CharacterCard(data4);
        Card ac7 = new CharacterCard(data4);
        Card ac8 = new CharacterCard(data3);
        Card ac9 = new CharacterCard(data3);
        Card ac10 = new CharacterCard(data2);
        Card ac11 = new CharacterCard(data2);
        Card ac12 = new CharacterCard(data1);
        Card ac13 = new CharacterCard(data1);
        cards2.Add(ac);
        cards2.Add(ac2);
        cards2.Add(ac3);
        cards2.Add(ac4);
        cards2.Add(ac5);
        cards2.Add(ac6);
        cards2.Add(ac7);
        cards2.Add(ac8);
        cards2.Add(ac9);
        cards2.Add(ac10);
        cards2.Add(ac11);
        cards2.Add(ac12);
        cards2.Add(ac13);
        cards2.Add(new Swindled());
        cards2.Add(new Swindled());
        cards2.Add(new Reinforce());
        cards2.Add(new Reinforce());

        Player playerOne = new Player(cards);
        Player playerTwo = new Player(cards2);
        BattleGround.startGame(playerOne, playerTwo);
	}
	

}
