﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_HeldDown : MonoBehaviour {
    public static bool left, right, up, down;

	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown("a")) {
            left = true;
        }
        if (Input.GetKeyDown("d")) {
            right = true;
        }
        if (Input.GetKeyDown("w")) {
            up = true;
        }
        if (Input.GetKeyDown("s")) {
            down = true;
        }
        if (Input.GetKeyUp("a")) {
            left = false;
        }
        if (Input.GetKeyUp("d")) {
            right = false;
        }
        if (Input.GetKeyUp("w")) {
            up = false;
        }
        if (Input.GetKeyUp("s")) {
            down = false;
        } 
	}
}
