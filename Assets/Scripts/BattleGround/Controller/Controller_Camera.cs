﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Camera : MyGameObject {
    private float zVel;
    private bool centered;

    void Start() {
        centerCamera();
    }

    void Update () {
        handleMovement();
        handleZoom();
    }

    private void centerCamera() {
        xVel = yVel = zVel = 0;
        xVel = -gameObject.GetComponent<Transform>().localPosition.x / 4;
        yVel = -(gameObject.GetComponent<Transform>().localPosition.y + getStandardZoom() * 0.65f) / 4; // 0.75f
        zVel = -(gameObject.GetComponent<Camera>().orthographicSize - getStandardZoom() * 1.75f) / 4; // 1.75f
    }

    private void handleZoom() {
        float mouseWheel = Input.GetAxis("Mouse ScrollWheel");
        if (mouseWheel != 0f) {
            zVel = -mouseWheel * (gameObject.GetComponent<Camera>().orthographicSize / 2);
        }
        gameObject.GetComponent<Camera>().orthographicSize += zVel;
        zVel *= 0.75f;
    }

    private void handleMovement() {
        if (Controller_HeldDown.left){
            Vector3 pos = gameObject.GetComponent<Transform>().localPosition;
            if(xVel > -0.25f)
                xVel += -0.05f;
        }
        if (Controller_HeldDown.right){
            Vector3 pos = gameObject.GetComponent<Transform>().localPosition;
            if (xVel < 0.25f)
                xVel += 0.05f;
        }
        if (Controller_HeldDown.up){
            Vector3 pos = gameObject.GetComponent<Transform>().localPosition;
            if (yVel > -0.25f)
                yVel += 0.05f;
        }
        if (Controller_HeldDown.down){
            Vector3 pos = gameObject.GetComponent<Transform>().localPosition;
            if (yVel < 0.25f)
                yVel += -0.05f;
        }
        if (Input.GetKeyDown("space") && Input.GetAxis("Mouse ScrollWheel") == 0) {
            centerCamera();
        }
    }

    private float getStandardZoom() {
        return (Board.size.y * 0.5f);
    }
}
