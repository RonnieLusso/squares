﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class View_SpellCard : View_Card {
    [SerializeField]
    private GameObject cardCost, cardName, cardText;

    protected override void setup_child(Card card) {
        SpellCard spellCard = (SpellCard)card;
        cardName.GetComponent<Text>().text = card.ToString();
        cardCost.GetComponent<Text>().text = "" + card.getCost();
        cardText.GetComponent<Text>().text = "" + spellCard.getInfo();
    }
}
