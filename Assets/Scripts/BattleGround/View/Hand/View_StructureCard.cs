﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class View_StructureCard : View_Card {
    [SerializeField]
    private GameObject cardCost, cardName, cardText, cardHealth;

    protected override void setup_child(Card card) {
        CharacterCard characterCard = (CharacterCard)card;
        cardCost.GetComponent<Text>().text = "" + characterCard.getCharacterData().getCost();
        cardName.GetComponent<Text>().text = characterCard.getCharacterData().getName();
        cardText.GetComponent<Text>().text = "" + characterCard.getCharacterData().getAbilitiesInfo();
        cardHealth.GetComponent<Text>().text = "" + characterCard.getCharacterData().getHealth();
    }

}
