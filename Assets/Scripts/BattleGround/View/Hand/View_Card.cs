﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class View_Card : MyGameObject {
    private Card card;
    private bool mousedOver;
    protected abstract void setup_child(Card card);

    public void setup(Card card, int index) {
        this.card = card;
        GameObject modelCard = GameObject.Find("Hand").GetComponent<View_Hand>().getModelCard();
        float scale = modelCard.GetComponent<Transform>().localScale.x;
        Vector3 modelCardPosition = modelCard.GetComponent<Transform>().localPosition;
        gameObject.GetComponent<Transform>().SetParent(GameObject.Find("Hand").GetComponent<Transform>());
        gameObject.GetComponent<Transform>().localScale = new Vector3(scale, scale, scale);
        gameObject.GetComponent<Transform>().localPosition = new Vector3((125 * index) + modelCardPosition.x, modelCardPosition.y, 0);
        gameObject.SetActive(true);
        setup_child(card);
    }

    public void onHoverEnter() {
        GameObject modelCard = GameObject.Find("Hand").GetComponent<View_Hand>().getModelCard();
        float scale = modelCard.GetComponent<Transform>().localScale.x;
        gameObject.GetComponent<Transform>().localScale = new Vector3(scale * 1.25f, scale * 1.25f, scale * 1.25f);
        yVel = 18;
    }

    public void onHoverExit() {
        yVel = xVel = 0;
        GameObject modelCard = GameObject.Find("Hand").GetComponent<View_Hand>().getModelCard();
        float scale = modelCard.GetComponent<Transform>().localScale.x;
        gameObject.GetComponent<Transform>().localScale = new Vector3(scale, scale, scale);
        gameObject.GetComponent<Transform>().localPosition = new Vector3(gameObject.GetComponent<Transform>().localPosition.x, modelCard.GetComponent<Transform>().localPosition.y, gameObject.GetComponent<Transform>().localPosition.z);
    }

    public void onClick() {
        Color c;
        View_Card activeCard = View_Hand.activeCard;
        if (activeCard == null) {
            c = gameObject.GetComponent<Image>().color;
            gameObject.GetComponent<Image>().color = new Color(c.r, c.g, c.b, 0.7f);
            View_Hand.activeCard = (gameObject.GetComponent<View_Card>());
            return;
        }
        if (activeCard == gameObject.GetComponent<View_Card>()) {
            c = gameObject.GetComponent<Image>().color;
            gameObject.GetComponent<Image>().color = new Color(c.r, c.g, c.b, 1f);
            View_Hand.activeCard = null;
            return;
        }
        c = activeCard.gameObject.GetComponent<Image>().color;
        activeCard.gameObject.GetComponent<Image>().color = new Color(c.r, c.g, c.b, 1f);
        c = gameObject.GetComponent<Image>().color;
        gameObject.GetComponent<Image>().color = new Color(c.r, c.g, c.b, 0.7f);
        View_Hand.activeCard = gameObject.GetComponent<View_Card>();
    }

    public Card getCard() {
        return card;
    }
}
