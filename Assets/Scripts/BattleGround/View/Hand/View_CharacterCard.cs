﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class View_CharacterCard : View_Card {
    [SerializeField]
    private GameObject cost, cardName, cardText, attack, health, movementType;

    protected override void setup_child(Card card) {
        CharacterCard characterCard = (CharacterCard)card;
        cost.GetComponent<Text>().text = ""  + characterCard.getCharacterData().getCost();
        cardName.GetComponent<Text>().text = characterCard.getCharacterData().getName();
        cardText.GetComponent<Text>().text = "" + characterCard.getCharacterData().getAbilitiesInfo();
        attack.GetComponent<Text>().text = "" + characterCard.getCharacterData().getAttack();
        health.GetComponent<Text>().text = "" + characterCard.getCharacterData().getHealth();
        if(characterCard.getCharacterData().getActionSet().Count > 0)
            movementType.GetComponent<Text>().text = "" + characterCard.getCharacterData().getActionSet()[0].ToString();
    }

}
