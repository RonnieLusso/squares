﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class View_Hand : MyGameObject {
    [SerializeField]
    private GameObject modelCard, characterCard, structureCard, spellCard;
    private List<View_Card> viewHand = new List<View_Card>();
    private List<Card> hand = new List<Card>();
    public static View_Card activeCard;

    void Start() {
        modelCard.SetActive(false);
    }

    void Update() {
        sizeDownCards();
        setupHand();
    }

    private void sizeDownCards() {
        if ((Input.mousePosition.y > ( Screen.height / 3.10))) {
            foreach (View_Card vc in viewHand) {
                vc.onHoverExit();
            }
        }
    }

    public static void resetActive() {
        try {
            Color c = activeCard.gameObject.GetComponent<Image>().color;
            activeCard.gameObject.GetComponent<Image>().color = new Color(c.r, c.g, c.b, 1f);
            activeCard = null;
        }
        catch (MissingReferenceException e) { }
        catch (NullReferenceException e) { }
    }

    public void playCard() {
        if (activeCard != null) {
            Card ac = activeCard.getCard();
            if (ac is CharacterCard || ac is TargetedSpellCard) {
                View_Square view_Square = getMousedSquare();
                if (view_Square != null) {
                    if (ac is CharacterCard) {
                        CharacterCard characterCard = (CharacterCard)ac;
                        characterCard.play(getMousedSquare().getSquare());
                    }
                    if (ac is TargetedSpellCard) {
                        TargetedSpellCard targetedSpellCard = (TargetedSpellCard)ac;
                        targetedSpellCard.play(getMousedSquare().getSquare());
                    }  
                }
            }
            if (ac is NonTargetedSpellCard) {
                NonTargetedSpellCard nonTargetedSpellCard = (NonTargetedSpellCard)ac;
                nonTargetedSpellCard.play();
            }
        }
    }

    private View_Square getMousedSquare() {
        List<View_Square> vSquares = GameObject.Find("Board").GetComponent<View_Board>().getVSquares();
        foreach (View_Square s in vSquares) {
            if (isMousedOver(s.gameObject)) {
                return s;
            }
        }
        return null;
    }

    private void setupHand() {
        var set = new HashSet<Card>(BattleGround.activePlayer.getHand());
        bool theSame = set.SetEquals(hand);
        if (!theSame) {
            foreach(View_Card c in viewHand){
                Destroy(c.gameObject);
            }
            hand = new List<Card>(BattleGround.activePlayer.getHand());
            viewHand = new List<View_Card>();
            for (int i = 0; i < hand.Count; i++) {
                GameObject gameObject = getGameObjectFromCard(hand[i]);
                View_Card card = gameObject.GetComponent<View_Card>();
                viewHand.Add(card);
                card.setup(hand[i], i);
            }
        }
    }

    private GameObject getGameObjectFromCard(Card card) {
        if (card is StructureCard) {
            return Instantiate(structureCard);
        }
        if (card is CharacterCard) {
            return Instantiate(characterCard);
        }
        if (card is SpellCard) {
            return Instantiate(spellCard);
        }
        throw new NullReferenceException();
    }

    public GameObject getModelCard() {
        return modelCard;
    }

    public List<View_Card> getViewHand(){
        return viewHand;
    }
}
