﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class View_CharacterHighlightTag : MonoBehaviour {
    [SerializeField]
    private GameObject health, level, attack, charName, abilities, movementType;
    
	
	// Update is called once per frame
	void Update () {
        if (View_Characters.activeCharacter != null) {
            health.GetComponent<Text>().text = "" + View_Characters.activeCharacter.getCharacter().getHealth();
            attack.GetComponent<Text>().text = "" + View_Characters.activeCharacter.getCharacter().attack;
            level.GetComponent<Text>().text = "" + View_Characters.activeCharacter.getCharacter().getCharacterData().getLevel();
            charName.GetComponent<Text>().text = "" + View_Characters.activeCharacter.getCharacter().getCharacterData().getName();
            abilities.GetComponent<Text>().text = "" + View_Characters.activeCharacter.getCharacter().getCharacterData().getAbilitiesInfo();
            if (View_Characters.activeCharacter.getCharacter().getCharacterData().getActionSet().Count > 0)
                movementType.GetComponent<Text>().text = "" + View_Characters.activeCharacter.getCharacter().getCharacterData().getActionSet()[0].ToString();
        } else {
            health.GetComponent<Text>().text = "N";
            attack.GetComponent<Text>().text = "N";
            level.GetComponent<Text>().text = "N";
            charName.GetComponent<Text>().text = "No Character";
            abilities.GetComponent<Text>().text = "";
            movementType.GetComponent<Text>().text = "";
        }
       
    }
}
