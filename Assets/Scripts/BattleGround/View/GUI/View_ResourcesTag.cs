﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class View_ResourcesTag : MonoBehaviour {
    [SerializeField]
    private GameObject text;

	// Update is called once per frame
	void Update () {
        text.GetComponent<Text>().text = "" + BattleGround.activePlayer.getResources();
    }
}
