﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class View_PlayerInfoTag : MyGameObject {
    [SerializeField]
    private GameObject player1CardCount, player1LandCount, player2CardCount, player2LandCount;

	// Update is called once per frame
	void Update () {
        player1CardCount.GetComponent<Text>().text = "" + (BattleGround.playerOne.getDeck().Count + BattleGround.playerOne.getHand().Count);
        player1LandCount.GetComponent<Text>().text = "" + BattleGround.getLandCount(BattleGround.playerOne);
        player2CardCount.GetComponent<Text>().text = "" + (BattleGround.playerTwo.getDeck().Count + BattleGround.playerTwo.getHand().Count);
        player2LandCount.GetComponent<Text>().text = "" + BattleGround.getLandCount(BattleGround.playerTwo);
    }
}
