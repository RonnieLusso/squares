﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class View_PlayerTag : MyGameObject {
    [SerializeField]
    private GameObject text;
    private int playerID;
	// Update is called once per frame
	void Update () {
        if (playerID != BattleGround.activePlayer.GetHashCode()) {
            playerID = BattleGround.activePlayer.GetHashCode();
            string name = (BattleGround.activePlayer.Equals(BattleGround.playerOne)) ? "One" : "Two";
            text.GetComponent<Text>().text = "Player " + name+ "'s Turn";
            text.GetComponent<Text>().color = BattleGround.activePlayer.getColor();
        }
	}
}
