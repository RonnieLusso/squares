﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class View_EndTurnButton : MyGameObject {
    [SerializeField]
    GameObject image;

    public void onHover() {
        
    }

    public void onClick() {
        BattleGround.nextTurn();
    }
}
