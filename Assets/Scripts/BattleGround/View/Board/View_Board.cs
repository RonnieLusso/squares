﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class View_Board : MyGameObject {
    [SerializeField]
    private GameObject modelSquare;
    private static List<View_Square> vSquares = new List<View_Square>();

	// Use this for initialization
	void Start () {
        foreach (Square s in Board.array) {
            View_Square vSquare = Instantiate(modelSquare).GetComponent<View_Square>();
            vSquare.setup(s);
            vSquares.Add(vSquare);
        }
    }

	public static View_Square getViewSquare(Square square) {
        foreach(View_Square vs in vSquares){
            if(vs.getSquare() == square){
                return vs;
            }
        }
        throw new SquareDoesNotExsistException();
	}

	// Update is called once per frame
	void Update () {

	}

    public List<View_Square> getVSquares() {
        return vSquares;
    }
}
