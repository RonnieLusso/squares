using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class View_Square : MyGameObject {
    [SerializeField]
    private GameObject resourceIndication;
    private Square square;
    private Player player;

    public void setup(Square square) {
        this.square = square;
        gameObject.GetComponent<Transform>().SetParent(GameObject.Find("Board").GetComponent<View_Board>().GetComponent<Transform>());
        setPosition(gameObject, square.getPoint().x * sizeOf(gameObject).x, square.getPoint().y * sizeOf(gameObject).y);
        setPosition(gameObject, positionOf(gameObject).x - (Board.size.x / 2 * sizeOf(gameObject).x), positionOf(gameObject).y - (Board.size.y / 2 * sizeOf(gameObject).y));
        if (square.getResourceWorth() < 1)
            resourceIndication.SetActive(true);
    }

    // Update is called once per frame
    void Update () {
        updateColor();
    }

    private void updateColor() {
        if(player != square.player){
            player = square.player;
            if(player == null){
                changeColorOf(gameObject, Color.white);
                return;
            }
            changeColorOf(gameObject, player.getColor());
        }
    }

    public Square getSquare() {
        return square;
    }
}
