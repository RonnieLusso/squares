﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class View_Indicator : MyGameObject {
    private Action action;
    private Square square;

    public void setup(Square square)  {
        Vector3 position = View_Board.getViewSquare(square).GetComponent<Transform>().localPosition;
        gameObject.GetComponent<Transform>().SetParent(GameObject.Find("Board").GetComponent<Transform>());
        gameObject.GetComponent<Transform>().localPosition = position;
    }

    public void setup(Square square, Action action)  {
        this.action = action;
        this.square = square;
        Vector3 position = View_Board.getViewSquare(square).GetComponent<Transform>().localPosition;
        gameObject.GetComponent<Transform>().SetParent(GameObject.Find("Board").GetComponent<Transform>());
        gameObject.GetComponent<Transform>().localPosition = position;
        
        changeAlphaOf(gameObject, 150 + (105 * Convert.ToInt32(View_Characters.activeCharacter.getCharacter().canMove(square, action))));
    }


    	// Update is called once per frame
	void Update () {
        try {
            if (isClicked(gameObject) && View_Characters.activeCharacter.getCharacter().canMove(square, action)) {
                onClick();
            }
        } catch (NullReferenceException) { }
	}

    private void onClick() {
        action.perform(View_Characters.activeCharacter.getCharacter());
        View_Characters.resetActive();
    }
}
