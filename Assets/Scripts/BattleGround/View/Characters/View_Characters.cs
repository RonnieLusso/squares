﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class View_Characters : MyGameObject {
    [SerializeField]
    private GameObject modelCharacter, modelStructure, attackIndicator, actionIndicator, highlightIndictor;
    private List<View_Character> viewCharacters = new List<View_Character>();
    private List<Character> characters = new List<Character>();
    private static List<GameObject> activeCharacterIndicators = new List<GameObject>();
    public static View_Character activeCharacter;
    private static bool showingActive;
    private static int playerID;
	
	// Update is called once per frame
	void Update () {
        nextTurn();
        setupCharacter();
        highlightActiveCharacter();
	}

    private void nextTurn() {
        if (playerID != BattleGround.activePlayer.GetHashCode()) {
            playerID = BattleGround.activePlayer.GetHashCode();
            resetActive();
        }
    }

    private void highlightActiveCharacter() {
        if(activeCharacter != null && !showingActive){
            showingActive = true;
            showAttackIndicators();
            showActionIndicators();
        }
        if (activeCharacter == null) {
            showingActive = false;
            foreach (GameObject g in activeCharacterIndicators) {
                Destroy(g);
            }
        }
    }

    private void showAttackIndicators() {
        foreach (Attack a in activeCharacter.getCharacter().getCharacterData().getAttackSet()) {
            foreach (Point p in a.getPoints()) {
                try {
                    Square square = Board.getSquare(Point.add(activeCharacter.getCharacter().square.getPoint(), p));
                    GameObject g = Instantiate(attackIndicator);
                    g.GetComponent<View_Indicator>().setup(square);
                    activeCharacterIndicators.Add(g);
                } catch (IndexOutOfRangeException) { }
            }
        }
    }

    private void showActionIndicators() {
        foreach (Action a in activeCharacter.getCharacter().getCharacterData().getActionSet()) {
            try {
                Square square = Board.getSquare(Point.add(activeCharacter.getCharacter().square.getPoint(), a.getPoint()));
                GameObject g = Instantiate(actionIndicator);
                g.GetComponent<View_Indicator>().setup(square, a);
                activeCharacterIndicators.Add(g);
            } catch (IndexOutOfRangeException) { }
        }
    }

    public static void resetActive() {
        activeCharacter = null;
        showingActive = false;
        foreach (GameObject g in activeCharacterIndicators) {
            Destroy(g);
        }
    }

    private void setupCharacter() {
        var set = new HashSet<Character>(BattleGround.getAllCharacters());
        bool theSame = set.SetEquals(characters);
        if (!theSame) {
            foreach(View_Character c in viewCharacters){
                Destroy(c.gameObject);
            }
            characters = BattleGround.getAllCharacters();
            viewCharacters = new List<View_Character>();
            for (int i = 0; i < characters.Count; i++) {
                GameObject gameObject = (characters[i] is Structure) ? Instantiate(modelStructure) : Instantiate(modelCharacter); 
                View_Character character = gameObject.GetComponent<View_Character>();
                viewCharacters.Add(character);
                character.setup(characters[i], i);
            }
        }
    }
}
