﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class View_Character : MyGameObject {
    [SerializeField]
    GameObject characterName, characterHealth, characterAttack, StatsImage;
    private Character character;
    private int squareID;
    
    public void setup(Character character, int index) {
        this.character = character;
        gameObject.GetComponent<Transform>().SetParent(GameObject.Find("Characters").GetComponent<Transform>());
        View_Square vs = View_Board.getViewSquare(character.square);
        gameObject.GetComponent<Transform>().localPosition = new Vector3(vs.GetComponent<Transform>().localPosition.x, vs.GetComponent<Transform>().localPosition.y, 0);
        gameObject.SetActive(true);
        characterName.GetComponent<Text>().text = character.getCharacterData().getName().Substring(0, 1);
        characterHealth.GetComponent<Text>().text = "" + character.getHealth();
        characterAttack.GetComponent<Text>().text = "" + character.attack;
        changeColorOf(gameObject, character.getPlayer().getColor());
        StatsImage.GetComponent<Image>().color = character.getPlayer().getColor();
    }
	
	// Update is called once per frame
	void Update () {
        positionCharacter();
        updateStats();
        highlight();
        if(isClicked(gameObject)){
            onClick();
        }
	}

    private void highlight() {
        if (View_Characters.activeCharacter == this) {
            Color c = new Color(244 / 255f, 206 / 255f, 66 / 255f);
            characterName.GetComponent<Text>().color = c;
            characterHealth.GetComponent<Text>().color = c;
            characterAttack.GetComponent<Text>().color = c;
        } else {
            characterName.GetComponent<Text>().color = Color.white;
            characterHealth.GetComponent<Text>().color = Color.white;
            characterAttack.GetComponent<Text>().color = Color.white;
        }
    }

    private void updateStats() {
        characterHealth.GetComponent<Text>().text = "" + character.getHealth();
        characterAttack.GetComponent<Text>().text = "" + character.attack;
    }

    private void positionCharacter() {
        if (squareID != character.square.GetHashCode()) {
            squareID = character.square.GetHashCode();
            View_Square vs = View_Board.getViewSquare(character.square);
            gameObject.GetComponent<Transform>().localPosition = new Vector3(vs.GetComponent<Transform>().localPosition.x, vs.GetComponent<Transform>().localPosition.y, 0);
        }
    }

    private void onClick() {
        View_Character activeCharacter = View_Characters.activeCharacter;
        if (!View_Hand.activeCard is SpellCard) {
            View_Hand.resetActive();
        }
        if (activeCharacter == null) {
            View_Characters.activeCharacter = this;
            return;
        }
        if (View_Characters.activeCharacter == this) {
            View_Characters.resetActive();
            return;
        }
        View_Characters.resetActive();
        View_Characters.activeCharacter = this;
    }

    public Character getCharacter() {
        return character;
    }
}
