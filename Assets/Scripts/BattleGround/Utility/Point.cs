﻿using UnityEngine;
using System.Collections;
using System;

public class Point {
    public int x;
    public int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public static Point add(Point a, Point b) {
        return new Point(a.x + b.x, a.y + b.y);
    }

    public static Point multiply(Point a, int num) {
        return new Point(a.x * num, a.y * num);
    }

    public override string ToString() {
        return "(" + x + ", " + y + ")";
    }
}
