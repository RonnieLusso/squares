﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MyGameObject : MonoBehaviour {
    protected float xVel, yVel;

    void LateUpdate() {
        gameObject.GetComponent<Transform>().localPosition += new Vector3(xVel, yVel, 0);
        xVel *= 0.75f;
        yVel *= 0.75f;
    }

    protected Vector3 positionOf(GameObject g) {
        return g.transform.position;
    }

    protected Vector3 sizeOf(GameObject g) {
        return g.GetComponent<Renderer>().bounds.size;
    }

    protected void setPosition(GameObject g, float x, float y) {
        g.GetComponent<Transform>().position = new Vector3(x, y, 0);
    }

    protected void translate(GameObject g, float x, float y) {
        g.GetComponent<Transform>().Translate(x, y, Time.deltaTime);
    }

    protected bool isMousedOver(GameObject g) {
        Vector3 mousePos = GameObject.Find("Main Camera").GetComponent<Camera>().ScreenToWorldPoint(Input.mousePosition);
        if (mousePos.x > (positionOf(g).x - sizeOf(g).x/2) && mousePos.x < positionOf(g).x + (sizeOf(g).x/2)) {
            if (mousePos.y > (positionOf(g).y - sizeOf(g).y / 2) && mousePos.y < positionOf(g).y + (sizeOf(g).y / 2)) {
                return true;
            }
        }
        return false;
    }

    protected float distanceBetween(Vector3 a, Vector3 b) {
        float dist = Mathf.Sqrt(Mathf.Pow(b.x - a.x, 2) + Mathf.Pow(b.y - a.y, 2));
        return dist;
    }

    protected bool isClicked(GameObject g) {
        return (isMousedOver(g) && Input.GetMouseButtonDown(0));
    }

    protected void show(GameObject g) {
        g.SetActive(true);
    }

    protected void hide(GameObject g) {
        g.SetActive(false);
    }

    protected void changeColorOf(GameObject g, float red, float blue, float green) {
        g.GetComponent<SpriteRenderer>().color = new Color(red/255, green/255, blue/255,
        g.GetComponent<SpriteRenderer>().color.a);
    }

    protected void changeColorOf(GameObject g, Color c) {
        Color newColor = new Color(c.r, c.g, c.b, g.GetComponent<SpriteRenderer>().color.a);
        g.GetComponent<SpriteRenderer>().color = newColor;
    }

    protected void changeAlphaOf(GameObject g, float alpha) {
        Debug.Log("this" + g);
        Color color = g.GetComponent<SpriteRenderer>().color;
        Color newColor = new Color(color.r, color.g, color.b, alpha/255);
        g.GetComponent<SpriteRenderer>().color = newColor;
    }

    protected void resetImageOf(GameObject g) {
        changeColorOf(g, 255, 255, 255);
        changeAlphaOf(g, 255);
    }

    protected List<GameObject> getChildrenOf(GameObject g) {
        List<GameObject> children = new List<GameObject>();
        foreach (Transform child in g.transform) {
            if (child.transform != g.transform) {
                children.Add(child.gameObject);
            }
        }
        return children;
    }

}
