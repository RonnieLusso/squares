﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManaShot : TargetedSpellCard {

    public override int getCost() {
        return 5;
    }

    public override string getInfo() {
        return "Deal damage equal to half of your current mana (" + (int)Math.Floor(BattleGround.activePlayer.getResources() / 2.0f) + ") ";
    }

    protected override bool isValidTarget(Square targetSquare) {
        return (targetSquare.isOccuppied());
    }

    protected override void performSpell(Square targetSquare) {
        Character character = BattleGround.getCharacter(targetSquare);
        character.appendHealth(-(int)Math.Floor(BattleGround.activePlayer.getResources() / 2.0f));
    }
       
}
