﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reinforce : TargetedSpellCard {

    public override int getCost() {
        return 3;
    }

    public override string getInfo() {
        return "Give a Character +1 Attack and +2 Health";
    }

    protected override bool isValidTarget(Square targetSquare) {
        return (targetSquare.isOccuppied());
    }

    protected override void performSpell(Square targetSquare) {
        Character character = BattleGround.getCharacter(targetSquare);
        character.maxHealth+=2;
        character.appendHealth(2);
        character.attack++;
    }
       
}
