﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Swindled : TargetedSpellCard {

    public override int getCost() {
        return 2;
    }

    public override string getInfo() {
        return "Change the nature of a characters abilities";
    }

    protected override bool isValidTarget(Square targetSquare) {
        return (targetSquare.isOccuppied());
    }

    protected override void performSpell(Square targetSquare) {
        Character character = BattleGround.getCharacter(targetSquare);
        List<Ability> onPlays = new List<Ability>(character.onDealthAbilities);
        List<Ability> onDealths = new List<Ability>(character.onPlayAbilities);
        List<Ability> passives = new List<Ability>();
        List<Ability> endTurns = new List<Ability>();
        List<Ability> standings = new List<Ability>();
        int randomIndex = UnityEngine.Random.Range(0, 2);
        Debug.Log(randomIndex);
        if (randomIndex == 1 || (character.endTurnAbilities.Count < 1 && character.passiveAbilities.Count > 0)) {
            passives = new List<Ability>(character.standingAbilities);
            standings = new List<Ability>(character.passiveAbilities);
        }
        if (randomIndex == 0 || (character.passiveAbilities.Count < 1 && character.endTurnAbilities.Count > 0)) {
            endTurns = new List<Ability>(character.standingAbilities);
            standings = new List<Ability>(character.endTurnAbilities);
        }

        foreach (Ability a in passives)
            a.setType(AbilityType.passive);
        foreach (Ability a in standings)
            a.setType(AbilityType.standing);
        foreach (Ability a in onPlays)
            a.setType(AbilityType.onPlay);
        foreach (Ability a in endTurns)
            a.setType(AbilityType.endTurn);
        foreach (Ability a in onDealths)
            a.setType(AbilityType.onDeath);

        character.passiveAbilities = passives;
        character.standingAbilities = standings;
        character.onPlayAbilities = onPlays;
        character.endTurnAbilities = endTurns;
        character.onDealthAbilities = onDealths;
    }
}
