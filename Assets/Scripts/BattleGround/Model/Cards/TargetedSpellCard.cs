﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

public abstract class TargetedSpellCard : SpellCard {

    protected abstract bool isValidTarget(Square targetSquare);

    protected abstract void performSpell(Square targetSquare);

    public void play(Square targetSquare) {
        if (isValidTarget(targetSquare) && getCost() <= BattleGround.activePlayer.getResources()) {
            performSpell(targetSquare);
            postPlay();
        }
    }
}
