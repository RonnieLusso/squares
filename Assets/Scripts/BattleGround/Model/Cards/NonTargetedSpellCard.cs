﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

public abstract class NonTargetedSpellCard : SpellCard {

    protected abstract void performSpell();

    public void play() {
        if(getCost() <= BattleGround.activePlayer.getResources()) {
            performSpell();
            postPlay();
        }
    }
}
