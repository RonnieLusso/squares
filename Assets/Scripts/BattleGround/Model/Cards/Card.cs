﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Card {

    public abstract int getCost();

    protected void postPlay() {
        BattleGround.activePlayer.getHand().Remove(this);
        BattleGround.activePlayer.setResources(BattleGround.activePlayer.getResources() - getCost());
    }
}
