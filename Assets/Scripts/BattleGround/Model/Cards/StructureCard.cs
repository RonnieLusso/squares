﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

public class StructureCard : CharacterCard {

    public StructureCard(CharacterData characterData) : base(characterData) { }

    public override void play(Square targetSquare) {
        if (isValidTarget(targetSquare) && getCost() <= BattleGround.activePlayer.getResources()) {
            Structure structure = new Structure(characterData, targetSquare, BattleGround.activePlayer);
            BattleGround.activePlayer.getTeam().Add(structure);
            postPlay();
        }
    }
}
