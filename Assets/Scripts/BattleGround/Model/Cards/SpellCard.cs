﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SpellCard : Card {

    public abstract string getInfo();

}
