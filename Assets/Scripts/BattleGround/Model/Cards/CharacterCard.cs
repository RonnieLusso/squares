﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

public class CharacterCard : Card {
    protected CharacterData characterData;

    public CharacterCard(CharacterData characterData) {
        this.characterData = characterData;
    }

    public override int getCost() {
        return characterData.getCost();
    }

    public virtual void play(Square targetSquare) {
        if (isValidTarget(targetSquare) && getCost() <= BattleGround.activePlayer.getResources()) {
            Character character = new Character(characterData, targetSquare, BattleGround.activePlayer);
            BattleGround.activePlayer.getTeam().Add(character);
            postPlay();
        }
    }

    protected bool isValidTarget(Square targetSquare) {
        return (!targetSquare.isOccuppied() && targetSquare.player == BattleGround.activePlayer);
    }

    public CharacterData getCharacterData() {
        return characterData;
    }
}
