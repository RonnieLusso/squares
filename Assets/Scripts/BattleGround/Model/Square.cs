﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Square {
    public Player player;
    private Point point;
    private int resourceWorth;

    public Square(Point point, int resourceWorth) {
        this.point = point;
        this.resourceWorth = resourceWorth;
    }

    public Point getPoint() {
        return point;
    }

    public bool isOccuppied() {
        return (BattleGround.getCharacter(this) != null);
    }

    public int getResourceWorth()  {
        return resourceWorth;
    }
}
