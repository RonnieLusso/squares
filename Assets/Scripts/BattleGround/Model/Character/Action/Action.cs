﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Action {
    protected Point point;

    public abstract void perform(Character character);

    public abstract bool isValid(Character character);

    public Action(Point point) {
        this.point = point;
    }

    public Point getPoint() {
        return point;
    }

    protected void postPerform(Character character) {
        character.hasMovedThisTurn = true;
        character.getPlayer().setResources(character.getPlayer().getResources() - 1);
    }

}
