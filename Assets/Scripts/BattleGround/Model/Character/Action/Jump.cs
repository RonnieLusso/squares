﻿
public class Jump : Action {

    public Jump(Point point) : base(point) { }

    public override bool isValid(Character character) {
        return (!Board.getSquare(Point.add(point, character.square.getPoint())).isOccuppied());
    }

    public override void perform(Character character) {
        Square targetSquare = Board.getSquare(Point.add(character.square.getPoint(), point));
        character.square = targetSquare;
        targetSquare.player = BattleGround.activePlayer;
        postPerform(character);
    }
}
