﻿
public class Transcend : Action {

    public Transcend(Point point) : base(point) { }

    public override bool isValid(Character character) {
        return (true);
    }

    public override void perform(Character character) {
        Square targetSquare = Board.getSquare(Point.add(character.square.getPoint(), point));
        character.square = targetSquare;
        postPerform(character);
    }
}
