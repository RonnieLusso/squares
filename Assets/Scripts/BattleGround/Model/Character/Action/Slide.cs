﻿
using System;
using System.Collections.Generic;
using UnityEngine;

public class Slide : Action {

    public Slide(Point point) : base(point) { }

    public override bool isValid(Character character) {
        bool isValid = true;
        foreach (Square s in getSquaresInbetween(character.square, Board.getSquare(Point.add(point, character.square.getPoint())))) {
            if (s.isOccuppied() && character.square != s) {
                isValid = false;
            }
        }
        return isValid;
    }

    public override void perform(Character character) {
        Square targetSquare = Board.getSquare(Point.add(character.square.getPoint(), point));
     
        foreach (Square s in getSquaresInbetween(character.square, targetSquare)) {
            s.player = BattleGround.activePlayer;
        }
        character.square = targetSquare;
        targetSquare.player = BattleGround.activePlayer;
        postPerform(character);
    }

    private List<Square> getSquaresInbetween(Square square, Square targetSquare) {
        List<Square> squaresInbetween = new List<Square>();
        int x = square.getPoint().x - targetSquare.getPoint().x;
        int y = square.getPoint().y - targetSquare.getPoint().y;
        if (x != 0 && y != 0) {
            for (int i = 0; i < Math.Abs(y); i++) {
                squaresInbetween.Add(Board.getSquare(new Point(square.getPoint().x + (i * -(Math.Abs(x) / x)), (square.getPoint().y + (i * -(Math.Abs(y)/ y))))));
            }
            return squaresInbetween;
        }
        if (x == 0) {
            for (int i = 0; i < Math.Abs(y); i++) {
                squaresInbetween.Add(Board.getSquare(new Point(square.getPoint().x, square.getPoint().y + (i * -(Math.Abs(y) / y)))));
            }
            return squaresInbetween;
        }
        if (y == 0) {
            for (int i = 0; i < Math.Abs(x); i++) {
                squaresInbetween.Add(Board.getSquare(new Point(square.getPoint().x + (i * -(Math.Abs(x) / x)), square.getPoint().y)));
            }
            return squaresInbetween;
        }
        throw new NullReferenceException();
    }

}
