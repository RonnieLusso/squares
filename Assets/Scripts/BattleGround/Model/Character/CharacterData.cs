﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterData {
    private string name;
    private int level;
    private int exp;
    private int attack;
    private int health;
    private int cost;
    private List<Action> actionSet;
    private List<Attack> attackSet;
    private List<Ability> abilitySet;

    public CharacterData(string name, int level, int exp, int attack, int health, int cost, List<Action> actionSet, List<Attack> attackSet, List<Ability> abilitySet) {
        this.name = name;
        this.level = level;
        this.attack = attack;
        this.health = health;
        this.cost = cost;
        this.actionSet = actionSet;
        this.attackSet = attackSet;
        this.abilitySet = abilitySet;
    }

    public string getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public int getAttack() {
        return attack;
    }

    public int getHealth() {
        return health;
    }

    public string getAbilitiesInfo() {
        string info = "";
        foreach (Attack a in attackSet) {
            info += "(Attack) " + a.ToString() + "\n\n";
        }
        foreach (Ability a in abilitySet) {
            info += "(" + a.getType() + ") " + a.ToString() + "\n\n";
        }
        return info;
    }

    public int getCost() {
        return cost;
    }

    public List<Action> getActionSet() {
        return actionSet;
    }

    public List<Attack> getAttackSet() {
        return attackSet;
    }

    public List<Ability> getAbilitySet() {
        return abilitySet;
    }
}
