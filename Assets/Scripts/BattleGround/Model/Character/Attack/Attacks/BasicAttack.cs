﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicAttack : Attack {

    public BasicAttack(List<Point> points) : base(points) { }

    protected override AttackTargets getWhoItTargets() {
        return AttackTargets.enemy;
    }

    protected override void perform_child(Character attacker, Character enemy) {
        enemy.appendHealth(-attacker.attack);
    }
}
