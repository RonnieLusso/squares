﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeSteal : Attack {

    public LifeSteal(List<Point> points) : base(points) { }

    protected override AttackTargets getWhoItTargets() {
        return AttackTargets.enemy;
    }

    protected override void perform_child(Character attacker, Character enemy) {
        enemy.appendHealth(-attacker.attack);
        attacker.appendHealth(attacker.attack);
    }

    public override string ToString() {
        return "Life Steal - Heals however much damage it inflicts";
    }
}
