﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Attack {
    private List<Point> points;

    protected abstract void perform_child(Character attacker, Character enemy);

    protected abstract AttackTargets getWhoItTargets();

    public Attack(List<Point> points) {
        this.points = points;
    }

    public void perform(Character attacker) {
        foreach(Point p in points){
            try {
                Square square = Board.getSquare(Point.add(attacker.square.getPoint(), p));
                Character enemy = BattleGround.getCharacter(square);
                attemptAttack(getWhoItTargets(), attacker, enemy);
            }
            catch (NullReferenceException) { }
            catch (IndexOutOfRangeException) { }
        }
    }

    private void attemptAttack(AttackTargets attackTargets, Character attacker, Character enemy) {
        if (attackTargets == AttackTargets.all) {
            perform_child(attacker, enemy);
        }
        if (attackTargets == AttackTargets.enemy && (enemy.getPlayer() != attacker.getPlayer())) {
            perform_child(attacker, enemy);
        }
        if (attackTargets == AttackTargets.friendly && (enemy.getPlayer() == attacker.getPlayer())) {
            perform_child(attacker, enemy);
        }
    }

    public List<Point> getPoints() {
        return points;
    }
}
