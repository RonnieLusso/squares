﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AttackTargets {
    friendly, enemy, all
}
