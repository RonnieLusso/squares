﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class CharacterLoader : MonoBehaviour {
    private static List<CharacterData> listOfCharacterdata = new List<CharacterData>();

    public static CharacterData getCharacterData(string characterName) {
        foreach (CharacterData cd in listOfCharacterdata) {
            if (cd.getName().Equals(characterName)) {
                return cd;
            }
        }
        throw new NullReferenceException(); 
    }


    void Awake() {
        
        FileInfo theSourceFile = null;
        StringReader reader = null;
        string txt;
        Debug.Log("fsdhfdsjhgfjsdgf");
        TextAsset puzdata = (TextAsset)Resources.Load("CharacterData", typeof(TextAsset));
        Debug.Log(puzdata.text);
        // puzdata.text is a string containing the whole file. To read it line-by-line:
        reader = new StringReader(puzdata.text);
        if (reader == null)
        {
            Debug.Log("puzzles.txt not found or not readable");
        }
        else
        {
            Debug.Log("fsdhfdsjhgfjsdgf");
            // Read each line from the file
            while ((txt = reader.ReadLine()) != null) {
                listOfCharacterdata.Add(parseLine(txt));
            }
                
        }

    }

    private CharacterData parseLine(string line) {
        string[] data = line.Split(',');
        string name = data[0].Trim();
        int attack; int.TryParse(data[1].Trim(), out attack);
        int health; int.TryParse(data[2].Trim(), out health);
        int cost; int.TryParse(data[3].Trim(), out cost);
        List<Action> actions = new List<Action>();
        List<Attack> attacks = new List<Attack>();
        List<Ability> abilities = new List<Ability>();
        try { actions = parseActions(data[4].Trim()); } catch (IndexOutOfRangeException) { }
        try { attacks = parseAttacks(data[5].Trim()); } catch (IndexOutOfRangeException) { }
        try { abilities = parseAbilities(data[6].Trim()); } catch (IndexOutOfRangeException) { }
        CharacterData characterData = new CharacterData(name, 1, 0, attack, health, cost, actions, attacks, abilities);
        return characterData;
    }

    private List<Ability> parseAbilities(string line) {
        List<Ability> abilities = new List<Ability>();
        string[] data = line.Split('/');
        foreach (string d in data) {
            System.Object a = System.Reflection.Assembly.GetExecutingAssembly().CreateInstance(d);
            Ability b = (Ability)a;
            abilities.Add(b);
        }
        return abilities;
    }

    private List<Attack> parseAttacks(string line) {
        string[] data = line.Split('/');
        List<Attack> attacks = new List<Attack>();
        foreach (string d in data) {
            string[] data2 = d.Split(':');
            string type = data2[0];
            List<Point> points = new List<Point>();
            foreach (string d2 in data2[1].Split('|')) {
                string[] cord = d2.Split('*');
                int x; int.TryParse(cord[0].Trim(), out x);
                int y; int.TryParse(cord[1].Trim(), out y);
                Point point = new Point(x, y);
                points.Add(point);
            }
            System.Object a = Activator.CreateInstance(Type.GetType(type), points);
            Attack b = (Attack)a;
            attacks.Add(b);
        }
        return attacks;
    }


    private List<Action> parseActions(string line) {
        string[] data = line.Split('/');
        List<Action> actions = new List<Action>();
        foreach (string d in data) {
            string[] data2 = d.Split(':');
            string type = data2[0];
            foreach (string d2 in data2[1].Split('|')) {
                string[] cord = d2.Split('*');
                int x;  int.TryParse(cord[0].Trim(), out x);
                int y; int.TryParse(cord[1].Trim(), out y);
                Point point = new Point(x, y);
                System.Object a = Activator.CreateInstance(Type.GetType(type), point);
                Action b = (Action)a;
                actions.Add(b);
            }
        }
        return actions;
    }
}
