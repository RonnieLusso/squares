﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Structure : Character{

    public Structure(CharacterData characterData, Square square, Player player) : base(characterData, square, player) { }

    
    public override bool canMove(Square square, Action action) {
        return false;
    }
}
