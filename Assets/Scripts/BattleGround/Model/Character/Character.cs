﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character {
    public List<Ability> allAbilities = new List<Ability>();
    public List<Ability> passiveAbilities = new List<Ability>();
    public List<Ability> standingAbilities = new List<Ability>();
    public List<Ability> endTurnAbilities = new List<Ability>();
    public List<Ability> onPlayAbilities = new List<Ability>();
    public List<Ability> onDealthAbilities = new List<Ability>();
    public List<Ability> activatedAbilities = new List<Ability>();
    public bool justPlayed = true;
    public bool hasMovedThisTurn;
    public Square square;
    public int maxHealth;
    public int attack;
    private int health;
    private CharacterData characterData;
    private Player player;
    private int stunnedFor;
    private bool performedActive;
    

    public Character(CharacterData characterData, Square square, Player player) {
        this.characterData = characterData;
        this.square = square;
        this.player = player;
        health = maxHealth = characterData.getHealth();
        attack = characterData.getAttack();
        setAbilities();
        performOnPlayAbility();
    }

    private void setAbilities() {
        foreach (Ability a in characterData.getAbilitySet()) {
            allAbilities.Add(a);
            if (a.getType() == AbilityType.onPlay)
                onPlayAbilities.Add(a);
            if (a.getType() == AbilityType.passive)
                passiveAbilities.Add(a);
            if (a.getType() == AbilityType.standing)
                standingAbilities.Add(a);
            if (a.getType() == AbilityType.endTurn)
                endTurnAbilities.Add(a);
            if (a.getType() == AbilityType.onDeath)
                onDealthAbilities.Add(a);
        }
    }

    public Player getPlayer() {
        return player;
    }

    public CharacterData getCharacterData() {
        return characterData;
    }

    public virtual bool canMove(Square square, Action action) {
        Debug.Log("testing");
        return (player.getResources() > 0 && stunnedFor < 1 && !square.isOccuppied() && !hasMovedThisTurn && !justPlayed && BattleGround.activePlayer == player && action.isValid(this));
    }

    public void performOnPlayAbility() {
        if (justPlayed) { 
            foreach (Ability a in onPlayAbilities) {
                a.perform(this);
            }
        }
    }

    public void performOnActivatedAbility() {
        foreach (Ability a in activatedAbilities) {
            a.perform(this);
        }
        performedActive = true;
    }

    public void performStandingAbility() {
        if (!justPlayed) { 
            if (!hasMovedThisTurn) {
                foreach (Ability a in standingAbilities) {
                    a.perform(this);
                }
            }
        }
    }

    public void performPassiveAbility() {
        if (!justPlayed) { 
            foreach (Ability a in passiveAbilities) {
                a.perform(this);
            }
        }
    }

    public void performEndTurnAbility() {
        if (!justPlayed) { 
            foreach (Ability a in endTurnAbilities) {
                a.perform(this);
            }
        }
    }

    public void performOnDealthAbility() {
        if (justPlayed) { 
            foreach (Ability a in onDealthAbilities) {
                 a.perform(this);
            }
        }
    }

    public void performAttack() {
        if (!justPlayed) {
            foreach (Attack a in characterData.getAttackSet()) {
                a.perform(this);
            }
        }
    }

    public void setStunnedFor(int i) {
        if (i < 1) {
            stunnedFor = 0;
            return;
        }
        stunnedFor -= i;
    }

    public int getStunnedFor() {
        return stunnedFor;
    }

    public void appendHealth(int i) {
        if (health + i > maxHealth) {
            health = maxHealth;
            return;
        }
        if (health + i < 1) {
            performOnDealthAbility();
            player.getTeam().Remove(this);
            health = 0;
            return;
        }
        health += i;
    }

    public int getHealth() {
        return health;
    }

    public bool hasPerformedActive() { 
        return performedActive;
    }

}
