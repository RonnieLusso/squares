﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Ability {
    private AbilityType abilityType;

    public abstract void perform(Character character);

    protected abstract string info();

    public Ability(AbilityType abilityType) {
        this.abilityType = abilityType;
    }

    public AbilityType getType() {
        return abilityType;
    }

    public void setType(AbilityType abilityType) {
        this.abilityType = abilityType;
    }

    public override string ToString() {
        return info();
    }
}
