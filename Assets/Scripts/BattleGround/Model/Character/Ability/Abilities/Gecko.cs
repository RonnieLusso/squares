﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gecko : Ability {

    public Gecko() : base(AbilityType.standing) { }

    public override void perform(Character character) {
        int a = character.attack;
        character.attack = character.getHealth();
        character.maxHealth = a;
        character.appendHealth(character.maxHealth);
    }

    protected override string info() {
        return "Swap attack and defense";
    }

}
