﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ladybug : Ability {

    public Ladybug() : base(AbilityType.endTurn) { }

    public override void perform(Character character) {
        heal(character, new Point(0, 1));
        heal(character, new Point(0, -1));
        heal(character, new Point(-1, 0));
        heal(character, new Point(1, 0));
        heal(character, new Point(-1, -1));
        heal(character, new Point(1, -1));
        heal(character, new Point(-1, 1));
        heal(character, new Point(1, 1));
    }

    protected override string info() {
        return "Heal all adjacent friendly characters equal to this characters Attack";
    }

    private void heal(Character character, Point p) {
        Square s = character.square;
        try {
            Square s2 = Board.getSquare(Point.add(p, s.getPoint()));
            Character c = BattleGround.getCharacter(s2);
            if (c != null && c.getPlayer() == character.getPlayer()) {
                c.appendHealth(character.attack);
            }
        } catch(IndexOutOfRangeException) { } 
    }

}
