﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arachnid : Ability {

    public Arachnid() : base(AbilityType.standing) { }

    public override void perform(Character character) {
        List<Square> options = getOptions(character);
        if (options.Count > 0) {
            int randomIndex = UnityEngine.Random.Range(0, options.Count - 1);
            Debug.Log(randomIndex);

            Square targetSquare = options[randomIndex];
            targetSquare.player = character.getPlayer();
            List<Action> actionSet = new List<Action>();
            actionSet.Add(new Jump(new Point(0, 1)));
            actionSet.Add(new Jump(new Point(1, 0)));
            actionSet.Add(new Jump(new Point(-1, 0)));
            actionSet.Add(new Jump(new Point(0, -1)));
            Character character3 = new Character(CharacterLoader.getCharacterData("Spider"), targetSquare, character.getPlayer());
            character.getPlayer().getTeam().Add(character3);
        }
       
    }

    private List<Square> getOptions(Character character) {
        List<Square> options = new List<Square>();
        try {
            Square s = Board.getSquare(Point.add(character.square.getPoint(), new Point(0, 1)));
            if (!s.isOccuppied())
                options.Add(s);
        } catch (IndexOutOfRangeException e) { }

        try {
            Square s = Board.getSquare(Point.add(character.square.getPoint(), new Point(0, -1)));
            if (!s.isOccuppied())
                options.Add(s);
        } catch (IndexOutOfRangeException e) { }

        try {
            Square s = Board.getSquare(Point.add(character.square.getPoint(), new Point(1, 0)));
            if (!s.isOccuppied())
                options.Add(s);
        } catch (IndexOutOfRangeException e) { }

        try {
            Square s = Board.getSquare(Point.add(character.square.getPoint(), new Point(-1, 0)));
            if (!s.isOccuppied())
                options.Add(s);
        } catch (IndexOutOfRangeException e) { }

        return options;
    }

    protected override string info() {
        return "Summon a (0,1) spider";
    }

}
