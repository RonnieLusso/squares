﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hornet : Ability {

    public Hornet() : base(AbilityType.standing) { }

    public override void perform(Character character) {
        
        character.appendHealth(-1);
    }

    protected override string info() {
        return "Take 1 Damage";
    }

}
