﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AbilityType {
    onPlay, onDeath, standing, passive, endTurn, activated
}
