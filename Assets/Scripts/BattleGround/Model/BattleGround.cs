﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleGround {
    public static Player playerOne, playerTwo, activePlayer;

    public static void startGame(Player playerOne, Player playerTwo) {
        BattleGround.playerOne = activePlayer = playerOne;
        BattleGround.playerTwo = playerTwo;
        Board.createBoard();
        Board.assignStartingSquares();
        assignStartingCards();
        activePlayer.startTurn();
    }

    public static int getResources() {
        int resources = 0;
        foreach (Square s in Board.array) {
            if (s.player == activePlayer) {
                resources += s.getResourceWorth();
            }
        }
        return Math.Max(0, resources - activePlayer.getTeam().Count);
    }

    public static int getLandCount(Player player) {
        int count = 0;
        foreach (Square s in Board.array) {
            if (s.player == player) {
                count++;
            }
        }
        return count;
    }

    public static Character getCharacter(Square square) {
        foreach (Character c in BattleGround.getAllCharacters()) {
            if (c.square == square) {
                return c;
            }
        }
        return null;
    }

    public static void nextTurn() {
        activePlayer.endTurn();
        activePlayer = (activePlayer.Equals(playerOne)) ? playerTwo : playerOne;
        activePlayer.startTurn();
    }

    private static void assignStartingCards() {
        // add shuffle card to player twos hand
    }

    public static List<Character> getAllCharacters(){
        List<Character> allCharacters = new List<Character>();
        foreach(Character c in playerOne.getTeam()){
            allCharacters.Add(c);
        }
        foreach (Character c in playerTwo.getTeam()) {
            allCharacters.Add(c);
        }
        return allCharacters;
    }
}
