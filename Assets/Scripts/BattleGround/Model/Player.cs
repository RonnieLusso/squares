using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Player {
    private List<Card> deck = new List<Card>();
    private List<Card> hand = new List<Card>();
    private List<Card> graveYard = new List<Card>();
    private List<Character> team = new List<Character>();
    private int resources;

    public Player(List<Card> deck) {
        this.deck = deck;
        shuffleCards(deck);
        drawCards(4);
    }

    public void startTurn() {
        drawCards(1);
        setResources(BattleGround.getResources());
        charactersStartTurn();
    }

    public void endTurn() {
        charactersEndTurn();
    }

    public void shuffleCards(List<Card> cards) {
        for (int i = 0; i < cards.Count; i++) {
            Card temp = cards[i];
            int randomIndex = UnityEngine.Random.Range(i, cards.Count);
            cards[i] = cards[randomIndex];
            cards[randomIndex] = temp;
        }
    }

    public void drawCard(Card card) {
        if (hand.Count > 3) {
            hand.Remove(hand[3]);
        }
        hand.Add(card);
    }

    public void drawCards(int amount) {
        for (int i = 0; i < amount; i++) {
            if (hand.Count > 3 || deck.Count < 1) {
                if (hand.Count > 3) {
                    //Card card = deck[0];
                    //deck.Remove(card);
                }
                if (deck.Count < 1 && !(hand.Count > 4)) {
                   // Card card = new fatige Card
                   //hand.Add(card);
                }
            } else {
                Card card = deck[0];
                deck.Remove(card);
                hand.Add(card);
            }
        }
    }

    private void charactersEndTurn() {
        List<Character> characters = new List<Character>(team);
        foreach (Character character in characters) {
            Debug.Log(character.getCharacterData().getName());
            character.performStandingAbility();
            character.performEndTurnAbility();
            character.performAttack();
            character.justPlayed = false;
            character.setStunnedFor(character.getStunnedFor() - 1);
        }
    }

    private void charactersStartTurn() {
        List<Character> characters = new List<Character>(team);
        foreach (Character character in characters) {
            character.performPassiveAbility();
            character.hasMovedThisTurn = false;
        }
    }

    public void setResources(int resources) {
        this.resources = resources;
    }

    public int getResources() {
        return resources;
    }

    public List<Card> getHand() {
        return hand;
    }

    public List<Card> getDeck() {
        return deck;
    }

    public List<Character> getTeam() {
        return team;
    }

    public Color getColor(){
        if (this == BattleGround.playerOne)
            return new Color(244/255f, 75/255f, 66/255f);

        return new Color(66/255f, 143/255f, 244/255f);
    }
}
