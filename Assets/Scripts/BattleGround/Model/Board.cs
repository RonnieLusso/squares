﻿using System;
using UnityEngine;

public class Board {
    public static Square[,] array;
    public static Point size = new Point(11, 6);

    public static void createBoard() {
        array = new Square[size.x, size.y];
        for (int i = 0; i < array.GetLength(0); i++) {
            for (int j = 0; j < array.GetLength(1); j++) {
                array[i,j] = new Square(new Point(i,j), determineWorth(i, j));
            }
        }
    }

    private static int determineWorth(int i, int j) {
        int x = 2;
        int y = 1;
        if (j < size.y - y && j > y - 1) {
            return 1;
        }
        return 0;
    }

    public static Square getSquare(Point point) {
        return array[point.x, point.y];
    }

    public static void assignStartingSquares() {
        int x = 2;
        int y = 2;
        foreach (Square s in array) {
            if (s.getPoint().y < size.y - y && s.getPoint().y > y - 1) {
                if (s.getPoint().x < (-(size.x - x) + size.x)) {
                    s.player = BattleGround.playerOne;
                }
                if (s.getPoint().x > size.x - (x + 1)) {
                    s.player = BattleGround.playerTwo;
                }
            }
        }
    }
}
